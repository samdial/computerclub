﻿namespace NM.Core.src.NM.Core.Database.Models
{
    public class User
    {
        public int Id { get; set; }
        public string? Identifier { get; set; }
        public string? Email { get; set; }
        public string? Login { get; set; }
        public string? Phone { get; set; }

        public string? PasswordHash { get; set; }
        public int RoleId { get; set; }
        public int WalletId { get; set; }
        public bool IsActive { get; set; }

        // Дополнительные поля
        public string? Name { get; set; }
        public float Money { get; set; }
    }
}